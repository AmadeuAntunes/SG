﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SG
{
    public partial class CFornecedores : Form
    {
        public CFornecedores()
        {
            
            InitializeComponent();
            List<Fornecedor> Fornecedor = Data.GetFornecedor();
            foreach (var item in Fornecedor)
            {
                dgFornecedores.Rows.Add(item.NomeFornecedor);
            }

            
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            Hide();
            Form Consultar= new Consultar();
            Consultar.Show();
        }
    }
}
