﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SG
{
    public class Fornecedor
    {
        public String NomeFornecedor { get; set; }
        public Fornecedor(string nomeFornecedor)
        {
            NomeFornecedor = nomeFornecedor;
        }
    }
}
