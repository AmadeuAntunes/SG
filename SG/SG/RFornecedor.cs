﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SG
{
    public partial class RFornecedor : Form
    {
        public RFornecedor()
        {
            InitializeComponent();
        }

        private void registarFornecedores_Click(object sender, EventArgs e)
        {
            if(txtNomeFornecedor.Text == String.Empty)
            {
                MessageBox.Show("Introduza o nome do Fornecedor");
            }
            else
            {

                Data.addFornecedor(new Fornecedor(txtNomeFornecedor.Text));
                MessageBox.Show("Fornecedor registado com processo");
                Hide();
                Form sg = new SG();
                sg.Show();
            }
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            Registar Registar = new Registar();
            Registar.Show();
            Hide();
        }
    }
}
