﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SG
{
    public partial class SG : Form
    {
        public SG()
        {
            InitializeComponent();
        }

        private void btnregistar_Click(object sender, EventArgs e)
        {
            Form Registar = new Registar();
            this.Hide();
            Registar.Show();
        }

        private void btnconsultar_Click(object sender, EventArgs e)
        {
            Form Consultar = new Consultar();
            this.Hide();
            Consultar.Show();
        }
    }
}
