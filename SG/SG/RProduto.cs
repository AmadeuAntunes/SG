﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace SG
{
    public partial class RProduto : Form
    {
        public RProduto()
        {
            InitializeComponent();
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            Hide();
            Form Registar = new Registar();
            Registar.Show();
        }

        private void registarProduto_Click(object sender, EventArgs e)
        {
            if(txtNomeProduto.Text == String.Empty)
            {
                MessageBox.Show("Introduza o nome do produto");
            }
            else
            {
               
                
                foreach (var item in Data.GetFornecedor())
                {
                    if (item.NomeFornecedor == cbFornecedor.SelectedItem.ToString())
                    {
                        Data.addProduto(new Produto(txtNomeProduto.Text, item));
                        MessageBox.Show("Produto introduzido com sucesso!");

                    }
                }
                Registar Registar = new Registar();
                Registar.Show();
                Hide();
            }
           
        }

        private void RProduto_Load(object sender, EventArgs e)
        {

            foreach (Fornecedor item in Data.GetFornecedor())
            {
                cbFornecedor.Items.Add(item.NomeFornecedor.ToString());
            }

            
            
        }
    }
}
