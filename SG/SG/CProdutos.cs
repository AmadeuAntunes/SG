﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SG
{
    public partial class CProdutos : Form
    {
        public CProdutos()
        {
            InitializeComponent();
            List<Produto> Produtos = Data.GetProduto();
            foreach (var item in Produtos)
            {
                dgProdutos.Rows.Add(item.produtoNome);
            }
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            Hide();
            Form Consultar = new Consultar();
            Consultar.Show();
        }
    }
}
