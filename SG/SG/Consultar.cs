﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SG
{
    public partial class Consultar : Form
    {
        public Consultar()
        {
            InitializeComponent();
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            Hide();
            Form SG = new SG();
            SG.Show();
        }

        private void btnFornecedores_Click(object sender, EventArgs e)
        {
            Hide();
            Form CFornecedores = new CFornecedores();
            CFornecedores.Show();
        }

        private void btnProdutos_Click(object sender, EventArgs e)
        {
            Hide();
            Form CProdutos = new CProdutos();
            CProdutos.Show();
        }
    }
}
