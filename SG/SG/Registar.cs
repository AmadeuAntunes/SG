﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SG
{
    public partial class Registar : Form
    {
        public Registar()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Hide();
            Form Fornecedores = new RFornecedor();
            Fornecedores.Show();
            
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            Form SG = new SG();
            SG.Show();
            Hide();
        }

        private void btnProdutos_Click(object sender, EventArgs e)
        {
            Hide();
            Form RProduto = new RProduto();
            RProduto.Show();
        }
    }
}
