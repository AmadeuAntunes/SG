﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SG
{
    public abstract class Data
    {
       private static List<Fornecedor> fornecedor = new List<Fornecedor>();
       private static List<Produto> produto = new List<Produto>();

        public static void addFornecedor(Fornecedor f)
        {
            fornecedor.Add(f);
        }
        public static void addProduto(Produto p)
        {
            produto.Add(p);
        }

        public static List<Fornecedor> GetFornecedor()
        {
            return fornecedor;
        }
        public static List<Produto> GetProduto()
        {
            return produto;
        }

    }
}
